#!/bin/bash

i3lock -c "#211c2b" \
    --verif-text="" \
    --wrong-text="" \
    --noinput-text="" \
    --ring-color="#3e3450" \
    --keyhl-color="#ec8fd0" \
    --ringver-color="#ec8fd0" \
    --insidever-color="#ec8fd0"
