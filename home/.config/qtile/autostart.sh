#!/bin/bash

# autostart
# kdeconnect-indicator &

# echo "sleep 10; OBS_USE_EGL=1 obs --startreplaybuffer --minimize-to-tray --profile 'game capture'" | bash & disown


xss-lock ~/Scripts/lock.sh &
xset -dpms
xset s off

eww daemon &

eww open bar &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

startup() {
    sleep 5
    wired --run &
    flameshot &
    localsend --hidden &
    ibus-daemon -drxR    
}

startup &

# x11 specific
if [ $XDG_SESSION_TYPE = "x11" ]; then
    
    python ~/.config/qtile/picom-watcher.py &
    ~/.xinitrc

fi

