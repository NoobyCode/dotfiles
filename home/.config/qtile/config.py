# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from palettes import regalis as colors

from libqtile.core.manager import Qtile
from libqtile import bar, layout, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy

mod = "mod4"
terminal = "alacritty"

(up, left, down, right) = "ijkl"

def toggle_picom(qtile):
    if os.system("pidof Q-picom-watcher") != 0:
        subprocess.Popen(["python", f"{os.environ["HOME"]}/.config/qtile/picom-watcher.py"])
    else:
        os.system("pkill -x Q-picom-watcher")
        os.system("pkill -x picom")

engines = ["xkb:us::eng", "xkb:us:intl:eng", "table:tokipona", ]

def switch_engine(qtile):
    current = subprocess.check_output(["ibus", "engine"]) \
                        .strip() \
                        .decode("utf-8")

    next_engine = engines[(engines.index(current) + 1) % len(engines)]
    os.system(f"ibus engine {next_engine}")

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "x", lazy.spawn(os.path.expanduser("~/Scripts/rofi/kill.sh")), desc="Open program killer"),
    Key([mod, "shift"], "s", lazy.spawn("flameshot gui"), desc="Take screenshot"),
    Key([mod], "b", lazy.spawn("rofi -show p -modi p:rofi-power-menu"), desc="Show power menu"),
    Key([mod], "period", lazy.spawn("rofi -show emoji"), desc="Open emoji picker"),
    Key([mod], "space", lazy.spawn("rofi -show drun"), desc="Open rofi launcher"),
    Key([mod], "p", lazy.function(toggle_picom), desc="Toggle compositor"),
    Key([mod], left, lazy.layout.left(), desc="Move focus to left"),
    Key([mod], right, lazy.layout.right(), desc="Move focus to right"),
    Key([mod], down, lazy.layout.down(), desc="Move focus down"),
    Key([mod], up, lazy.layout.up(), desc="Move focus up"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], left, lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], right, lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], down, lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], up, lazy.layout.shuffle_up(), desc="Move window up"),
    Key([], "Control_R", lazy.function(switch_engine), desc="Next keyboard layout."),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], left, lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], right, lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], down, lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], up, lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control", "shift"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    # Key([mod], "space")
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

groups += [
    ScratchPad("popupterm", [
        DropDown("term", terminal, height=0.5, width=0.5, y=0.25, x=0.25, warp_pointer=False, on_focus_lost_hide=False)
    ])
]

keys.extend([
    Key([mod], "t", lazy.group["popupterm"].dropdown_toggle("term"), desc="Open Popup Terminal"),
])

border_conf = {
    "margin": 3,
    "border_width": 2,
    "border_focus": str(colors.focused),
    "border_normal": str(colors.unfocused)
}

layouts = [
    layout.Columns(
        border_on_single=True,
        **border_conf
    ),
    layout.Max(
        **border_conf
    )
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

bar_font = "Hack Nerd Font, Regular"

bar_height = 31

screens = [
    Screen(
        wallpaper="~/Wallpapers/regalis-3.png",
        wallpaper_mode="fill",
        top=bar.Gap(33),
        left=bar.Gap(3),
        right=bar.Gap(3),
        bottom=bar.Gap(3),
    ),
    Screen(
        wallpaper="~/Wallpapers/planepic.jpg",
        wallpaper_mode="fill"
    ),
]

def drag(qtile: Qtile, x, y):
    qtile.current_window.set_position_floating(x, y)

# Drag floating layouts.
mouse = [
    Click([mod, "control"], "Button1", lazy.window.toggle_floating()),
    Drag([mod, "shift"], "Button1", lazy.function(drag), start=lazy.window.get_position()),
    Drag([mod, "shift"], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="gcolor3"),
        Match(wm_class="Godot_Engine"),
        Match(wm_class="ibus-setup-table"),
    ],
    **border_conf
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.

wmname = "LG3D"


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

os.system("~/.xinitrc")
os.system("eww reload")

# reload eww bar when reloading the config to put it on top of the bar
# todo: figure out how to eliminate unnecessary reload on initial startup
# subprocess.call(["eww", "reload"])
