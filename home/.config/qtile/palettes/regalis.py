from widgets import MyWttr
import os
from libqtile import bar, widget
# from qtile_extras.widget.decorations import PowerLineDecoration, BorderDecoration, bar

font_color = "#EBEBFF"
baricon = "#ec8fd0"
unfocused = "#2B2438"
focused = "#5F507C" 
bar0 = "#18141Faa"
bar1 = "#2B2438"
bar2 = "#3E3450"
bariconfg = bar0
baricon2 = "#39abdc"
baricon3 = "#E384F2"

def toggle_rofi():
    s = os.system("rofi -show drun")
    if s != 0:
        os.system("pkill rofi")

def toggle_mixer():
    mixer_visible = os.system("eww windows | grep '*settings-menu'") == 0
    if not mixer_visible:
        os.system("eww open settings-menu")
    else:
        os.system("eww close settings-menu")
        os.system("eww update currentTab='primary'")

start_button = {
    "mouse_callbacks": {
        "Button1": toggle_rofi
    }
}

mixer_button = {
    "mouse_callbacks": {
        "Button1": toggle_mixer
    }
}

bar_font = "Hack Nerd Font, monospace"
bar_height = 30

topbar = bar.Bar(
    [
        # widget.Spacer(
        #     10,
        #     background=baricon,
        #     **start_button
        # ),
        # widget.TextBox(
        #     text="\uf303 ",
        #     fonts=bar_font,
        #     fontsize=bar_height - 10,
        #     foreground=bar0,
        #     background=baricon,
        #     **start_button,
        # ),
        # widget.Spacer(
        #     1,
        #     background=baricon,
        #     **powerline,
        #     **start_button
        # ),
        # widget.Clock(
        #     format="%A %Y-%m-%d %H:%M %Z |",
        #     font=bar_font,
        #     foreground=font_color,
        #     background=bar2
        # ),
        # MyWttr(
        #     lang='en',
        #     background=bar2,
        #     foreground=font_color,
        #     font=bar_font,
        #     location={
        #         '': '',
        #     },
        #     format='%c%t \ufa8b %p',
        #     units='m',
        #     update_interval=60,
        #     **powerline
        # ),
        # widget.CurrentLayout(
        #     font=bar_font,
        #     background=bar1,
        #     foreground=font_color,
        # ),
        # widget.CurrentLayoutIcon(
        #     font=bar_font,
        #     background=bar1,
        #     foreground=font_color,
        #     scale=0.6,
        #     **powerline,
        # ),
        widget.KeyboardLayout(
            background=bar0,
            foreground="#00000000",
            configured_keyboards=['us', 'us intl']
        ),
        # widget.Spacer(background=bar0),
        # widget.Spacer(15, background=bar0),
        # widget.GroupBox(
        #     disable_drag=True,
        #     this_current_screen_border=baricon,
        #     highlight_color=[bar0, "9B0B6F"],
        #     background=bar0,
        #     foreground=font_color,
        #     other_screen_border="#444444",
        #     inactive="#777777",
        #     **powerline,
        #     highlight_method="line",
        #     rounded=False
        # ),
        widget.Spacer(background=bar0),
        # widget.Systray(background=bar0),
        # widget.Spacer(5, background=baricon3, **mixer_button),
        # widget.TextBox(
        #     text="\uf013 ",
        #     fontsize=bar_height - 10,
        #     fonts=bar_font,
        #     foreground=bar0,
        #     background=baricon3,
        #     **mixer_button
        # ),
        widget.Spacer(118, background=bar0)
    ],
    30,
    background="#FF000000",
    margin=[0, 0, 3, 0],
    opacity=1,
 ) 
