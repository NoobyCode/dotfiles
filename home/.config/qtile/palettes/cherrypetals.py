
from widgets import MyWttr
from libqtile import bar, widget

unfocused = "#7e340a"
focused = "#912b34"
bar0 = "#140E0C"
bar1 = "#251916"
bar2 = "#3A2722"
baricon = "#FF5E98"
bariconfg = bar0
baricon2 = "#39abdc"
baricon3 = "#F3CD59"

def toggle_rofi():
    s = os.system("rofi -show drun")
    if s != 0:
        os.system("pkill rofi")

start_button = {
    "mouse_callbacks": {
        "Button1": toggle_rofi
    }
}

bar_font = "Hack Nerd Font, monospace"
bar_height = 30

topbar = bar.Bar(
    [
        widget.Spacer(
            10,
            background=baricon,
            **start_button
        ),
        widget.TextBox(
            text="\uf303",
            fonts=bar_font,
            fontsize=bar_height,
            foreground=bar0,
            background=baricon,
            **start_button
        ),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar2,
            background=baricon,
            padding=0,
            fontsize=bar_height,
            **start_button
        ),
        widget.Clock(
            format="%A %Y-%m-%d %H:%M %Z |",
            font=bar_font,
            background=bar2
        ),
        MyWttr(
            lang='en',
            background=bar2,
            font=bar_font,
            location={
                '': '',
            },
            format='%c%t \ufa8b %p',
            units='m',
            update_interval=60,
        ),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar1,
            background=bar2,
            padding=0,
            fontsize=bar_height
        ),
        widget.CurrentLayout(
            font=bar_font,
            background=bar1,
        ),
        widget.CurrentLayoutIcon(
            font=bar_font,
            background=bar1,
            scale=0.6
        ),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar0,
            background=bar1,
            padding=0,
            fontsize=bar_height
        ),
        widget.KeyboardLayout(
            background=bar0,
            configured_keyboards=['us', 'us intl']
        ),
        widget.Prompt(font=bar_font, background=bar0),
        widget.Spacer(background=bar0),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar2,
            background=bar0,
            padding=0,
            fontsize=bar_height
        ),
        widget.GroupBox(
            disable_drag=True,
            this_current_screen_border=focused,
            this_screen_border=focused,
            background=bar2,
            other_screen_border="#555555",
            inactive="#aaaaaa"
            # rounded=True
        ),
        widget.TextBox(
            text="\ue0bc ",
            fonts=bar_font,
            foreground=bar2,
            background=bar0,
            padding=0,
            fontsize=bar_height
        ),
        widget.Spacer(background=bar0),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar1,
            background=bar0,
            padding=0,
            fontsize=bar_height
        ),
        widget.Systray(background=bar1),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=bar2,
            background=bar1,
            padding=0,
            fontsize=bar_height
        ),
        widget.Spacer(5, background=bar2),
        widget.Volume(
            channel="Capture",
            font=bar_font,
            fmt="\uf130 {}",
            background=bar2
        ),
        widget.Volume(
            channel="Master",
            font=bar_font,
            fmt="\ufa7d {}",
            background=bar2
        ),
        widget.TextBox(
            text="\ue0ba",
            fonts=bar_font,
            foreground=baricon3,
            background=bar2,
            padding=0,
            fontsize=bar_height
        ),
        widget.QuickExit(
            background=baricon3,
            foreground=bariconfg,
            countdown_format="{}",
            default_text="\u23fb",
            fontsize=bar_height - 10,
            padding=10
        ),
        widget.Spacer(5, background=baricon3)
    ],
    30,
    background="#FF0000",
    margin=[0, 0, 3, 0],
    opacity=1,
 ) 
