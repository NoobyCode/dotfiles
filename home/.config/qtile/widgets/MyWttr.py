from libqtile.log_utils import logger
from libqtile import widget
import re


class MyWttr(widget.Wttr):
    # My own rewritten version of parse that reduces whitespace
    # If wttr.in responds with something like:
    # ☀️   +24C
    # It will convert the text to
    # ☀️ +24C
    # which looks nicer
    def poll(self):
        p = super().poll()

        if p == "No network":
            return "wttr.in is borked &gt;˷&lt;"
        return self.parse(p)
        

    def parse(self, response):
        for coord in self.location:
            response = response.strip().replace(coord, self.location[coord])
        response = re.sub(' +', ' ', response)
        # Apparently, if wttr.in returns an error, it displays my exact geological coordinates!?
        logger.debug(response)
        if "Unknown location;" in response:
            response = "wttr.in is borked &gt;˷&lt;"
        return response
