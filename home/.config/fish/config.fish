
# Greeting

function fish_greeting
    set greetings   "time to make some breaking changes!" \
                    "try not to break anything, ok?" \
                    "welcome home!" \
                    "all humans are equal before fish(shell)." \
                    "any great ideas?" \
                    "how clean is your room?" \
                    "you forgot to eat or drink, did you?" \
                    "penguins!! 🐧🐧🐧" \
                    "don't forget to take a break and stretch!!" \
                    "can this be automated?" \
                    "has someone done this before?" \
                    "keep it simple, stupid!!" \
                    "git commit before it gets messy!" \
                    "󱥄 󱥩 󱥴　󱥄 󱥉 󱤉 󱤄　󱥞 󱤘 󱥩 󱤴 !" \
                    "sina kasi anu seme?" \
                    "poki nanpa kala li epiku a!" \
    
    if not set -q __LAST_USED_GREETING
        set -U __LAST_USED_GREETING 0
    end

    set pool (seq 1 (count $greetings))

    set -e pool[$__LAST_USED_GREETING]

    set -U __LAST_USED_GREETING $pool[(random 1 (count $pool))]

    echo
    set_color magenta
    echo -e $greetings[$__LAST_USED_GREETING]
end

# Variables

## Editor
export EDITOR="nvim"
export VISUAL="nvim"

## Tide Prompt Config
set -g tide_character_icon "%"
set -g tide_character_color "normal"
set -g tide_character_color_failure "red"
set -g tide_status_icon_failure ""
set -g tide_left_prompt_items pwd git newline character
set -g tide_cmd_duration_decimals 2
set -g tide_cmd_duration_color yellow

# Interactive

if status is-interactive
    # Syntax Highlighting Colors
    set -g fish_color_normal white
    set -g fish_color_command cyan
    set -g fish_color_keyword cyan
    set -g fish_color_quote green
    set -g fish_color_redirection brcyan
    set -g fish_color_end red
    set -g fish_color_error brred
    set -g fish_color_param red
    set -g fish_color_comment brblack
    set -g fish_color_operator magenta
    set -g fish_color_escape red
    set -g fish_color_autosuggestion brblack
end

# Aliases

## Programs
alias ls="exa"
alias la="exa -lab --octal-permissions"
alias tree="exa -T"
alias cat="bat --theme ansi -P"
alias pcat="bat --theme ansi --paging always"
alias lg="lazygit"
alias rm!="command rm"
alias !!="sudo (eval echo \$history[1])"
alias lock="loginctl lock-session \$XDG_SESSION_ID"

## Directories
alias ~work="cd ~/Documents/Work"
alias ~projects="cd ~/Documents/Work/Projects"
alias ~clone="cd ~/Documents/Work/GitClone"
alias ~projects="cd ~/Documents/Work/Projects"
alias ~whims="cd ~/Documents/Work/Whims"

# Functions

function steam-compatdata -d "cd into the config directory of a game that uses Proton"
    set appid (grep -i ~/.local/share/Steam/steamapps/appmanifest* -e $argv[1] | head -1 | choose 0 | sed -e "s/[^0-9]//g") &&
    
    cd ~/.local/share/Steam/steamapps/compatdata/$appid/pfx/drive_c/users/steamuser/AppData/ &&

    tree -L 2 -I "Microsoft"
end

function rm -d "alias rm for trash, and add a warning too"
    set_color yellow
    echo 'note: `rm` is aliased to `trash`'
    echo 'if you really wanna permanently delete a file, use `rm!`'
    echo
    set_color normal
    trash -i $argv
end
