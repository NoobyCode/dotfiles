
local lib = require("custom.lib")

vim.cmd([[
" Neovide
set guifont=Hack\ Nerd\ Font:h12
" Tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
let g:pyindent_open_paren=shiftwidth()

]])

