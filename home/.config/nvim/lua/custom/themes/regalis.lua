local M = {}

M.base_30 = {
  white = "#D9E0EE",
  darker_black = "#211C2B",
  black = "#2B2438", --  nvim bg
  black2 = "#342C44",
  one_bg = "#18141F", -- real bg of onedark
  one_bg2 = "#363545",
  one_bg3 = "#3e3d4d",
  grey = "#474656",
  grey_fg = "#645d74",
  grey_fg2 = "#555464",
  light_grey = "#f2c5e0",
  red = "#F38BA8",
  baby_pink = "#ffa5c3",
  pink = "#F5C2E7",
  line = "#383747", -- for lines like vertsplit
  green = "#ec8fd0",
  vibrant_green = "#b6f4be",
  nord_blue = "#f2c5e0",
  blue = "#89B4FA",
  yellow = "#FAE3B0",
  sun = "#ffe9b6",
  purple = "#ec8fd0",
  dark_purple = "#ec8fd0",
  teal = "#B5E8E0",
  orange = "#F8BD96",
  cyan = "#89DCEB",
  statusline_bg = "#18141f",
  lightbg = "#2B2438",
  pmenu_bg = "#ABE9B3",
  folder_bg = "#89B4FA",
  lavender = "#c7d1ff",
}

M.base_16 = {
  base00 = "#211C2B",
  base01 = "#2B2438",
  base02 = "#473C5D",
  base03 = "#3E3450",
  base04 = "#8a76b5",
  base05 = "#EBEBFF",
  base06 = "#EBEBFF",
  base07 = "#EBEBFF",
  base08 = "#F38BA8",
  base09 = "#f2c5e0",
  base0A = "#fbe7c6",
  base0B = "#ABE9B3",
  base0C = "#89DCEB",
  base0D = "#ec8fd0",
  base0E = "#f2c5e0",
  base0F = "#F38BA8",
}

M.polish_hl = {}

M.type = "dark"

return M
