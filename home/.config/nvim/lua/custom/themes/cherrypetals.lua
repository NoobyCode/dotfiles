local M = {}

M.base_30 = {
   white = "#abb2bf",
   darker_black = "#150f0f",
   black = "#140E0C", --  nvim bg
   black2 = "#3c2a2a",
   one_bg = "#140E0C", -- real bg of onedark
   one_bg2 = "#261c1c",
   one_bg3 = "#261c1c",
   grey = "#42464e",
   grey_fg = "#565c64",
   grey_fg2 = "#6f737b",
   light_grey = "#6f737b",
   red = "#d47d85",
   baby_pink = "#DE8C92",
   pink = "#ff75a0",
   line = "#946c6c", -- for lines like vertsplit
   green = "#A3BE8C",
   vibrant_green = "#7eca9c",
   nord_blue = "#81A1C1",
   blue = "#61afef",
   yellow = "#e7c787",
   sun = "#EBCB8B",
   purple = "#b4bbc8",
   dark_purple = "#c882e7",
   teal = "#519ABA",
   orange = "#fca2aa",
   cyan = "#a3b8ef",
   statusline_bg = "#140f0f",
   lightbg = "#3a2c2c",
   lightbg2 = "#342727",
   pmenu_bg = "#A3BE8C",
   folder_bg = "#ff597d",
}
--#56c288#39abdc#39abdc
M.base_16 = {
   base00 = "#251a19",
   base01 = "#4a5f6a",
   base02 = "#513d3d",
   base03 = "#545862",
   base04 = "#565c64",
   base05 = "#bdc3c7",
   base06 = "#b6bdca",
   base07 = "#c8ccd4",
   base08 = "#ff597d",
   base09 = "#d19a66",
   base0A = "#39abdc",
   base0B = "#e0b053",
   base0C = "#39abdc",
   base0D = "#ffe185",
   base0E = "#c25185",
   base0F = "#bdc3c7",
}

return M
