local M = {}

M.base_30 = {
   white = "#abb2bf",
   darker_black = "#090911",
   black = "#242642", --  nvim bg
   black2 = "#282A49",
   one_bg = "#242642", -- real bg of onedark
   one_bg2 = "#242642",
   one_bg3 = "#242642",
   grey = "#42464e",
   grey_fg = "#565c64",
   grey_fg2 = "#6f737b",
   light_grey = "#6f737b",
   red = "#d47d85",
   baby_pink = "#DE8C92",
   pink = "#ff75a0",
   line = "#454A87", -- for lines like vertsplit
   green = "#A3BE8C",
   vibrant_green = "#7eca9c",
   nord_blue = "#81A1C1",
   blue = "#61afef",
   yellow = "#e7c787",
   sun = "#EBCB8B",
   purple = "#b4bbc8",
   dark_purple = "#c882e7",
   teal = "#519ABA",
   orange = "#fca2aa",
   cyan = "#a3b8ef",
   statusline_bg = "#090911",
   lightbg = "#242642",
   lightbg2 = "#282A49",
   pmenu_bg = "#A3BE8C",
   folder_bg = "#F3CD59",
}

M.base_16 = {
   base00 = "#121321",
   base01 = "#4a5f6a",
   base02 = "#363963",
   base03 = "#545862",
   base04 = "#565c64",
   base05 = "#bdc3c7",
   base06 = "#b6bdca",
   base07 = "#c8ccd4",
   base08 = "#FF5E98",
   base09 = "#6FA6D9",
   base0A = "#66D354",
   base0B = "#e0b053",
   base0C = "#6FA6D9",
   base0D = "#F3CD59",
   base0E = "#6FA6D9",
   base0F = "#bdc3c7",
}

return M
