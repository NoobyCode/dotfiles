
local module = {}

-- my own version of os.execute that returns the stdout of the command
-- BUG: isSuccessful always returns true in neovim. Tested in lua repl, and it returns false on fail
-- BUG: idk why this happens, but do not rely on isSuccessful
-- returns: output, isSuccessful?
function module.execute(cmd)
    local f = io.popen(cmd, 'r')
    local s = f:read('*a')
    local _, _, code = f:close()
    s = string.gsub(s, '^%s+', '')
    s = string.gsub(s, '%s+$', '')
    s = string.gsub(s, '[\n\r]+', ' ')
    return s, (code == 0)
end

function module.isInRepo()
    return module.execute("git rev-parse --is-inside-work-tree") == "true"
end

function module.openRepo()
    local url = module.execute("git remote get-url origin 2>&1")

    -- If string starts with fatal or error, assume we dont have a git repo
    local sub = string.find(url, "[fatal: |error: ]")
    if sub == 1 then
        print("Could not find an origin URL")
        return
    end

    -- If url is an ssh url, turn it into a valid browser url
    local index = string.find(url, "@")
    if index then
        -- lua does not have a split function for some ungodly reason
        url = string.sub(url, index+1, #url)
        url = string.gsub(url, ":", "/")
        url = string.gsub(url, ".git", "")
        url = "https://"..url
    end

    -- Prompt user if they want to open the url in a browser
    local input = vim.fn.input(string.format("Open %s in browser? (y/N): ", url))
    if string.match(string.lower(input), "[y|yes]") then
        module.execute("xdg-open "..url.." &>/dev/null & disown")
    end
end

function module.clamp(min, x, max)
    return math.min(math.max(x, min), max)
end

return module
