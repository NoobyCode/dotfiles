
local module = {}

module.plugins = "custom.plugins"

module.ui = {
    theme = "regalis",
    cmp = {
        style = "atom_colored", -- default/flat_light/flat_dark/atom/atom_colored
    },
    cheatsheet = { theme = "simple" },
}

module.mappings = {
    general = {
        n = {
            ["<leader>b"] = {"<Cmd>NvimTreeToggle<CR>"}, -- Overrides NvChad's <leader>b binding
            ["<leader>ch"] = {"<Cmd>lua vim.lsp.buf.hover()<CR>", "Hover"},
            ["<leader>cs"] = {"<Cmd>NvCheatsheet<CR>", "cheatsheet"},
        },
        i = {
            ["<C-BS>"] = {"<C-w>"}
        }
    },
    code = {
        n = {
            ["<leader>cd"] = {"<Cmd>lua vim.lsp.buf.definition()<CR>", "Go to definition"},
            ["<leader>ca"] = {"<Cmd>lua vim.lsp.buf.code_action()<CR>", "Code action"},
            ["<leader>cr"] = {
                function ()
                    require("nvchad_ui.renamer").open()
                end, "Rename"},
            ["<leader>ce"] = {
                function ()
                    vim.diagnostic.open_float()
                end, "Open diagnostics"}
        }
    },
    util = {
        n = {
            ["<C-Right>"] = {"w"},
            ["<C-Left>"] = {"b"},
            ["<C-Up>"] = {"<C-y>"},
            ["<C-Down>"] = {"<C-e>"},
            ["<leader>;"] = {"<Cmd>lua os.execute('alacritty & disown')<CR>", "Open Alacritty"},
            ["<leader>gh"] = {"<Cmd>lua require('custom.lib').openRepo()<CR>", "Open origin repo"},
            ["<leader><leader><leader>"] = {"<Cmd>LspRestart<CR>", "Restart LSP"}
        }
    },
    find = {
        n = {
            ["<leader>lg"] = {"<Cmd>Telescope live_grep<CR>", "Telescope LiveGrep"}
        }
    },
    dap = {
        n = {
            ["B"] = {
                function()
                    require'dap'.toggle_breakpoint()
                end
            },
            ["]"] = {
                function()
                    require'dap'.step_over()
                end
            },
            ["}"] = {
                function()
                    require'dap'.step_into()
                end
            },
            ["C"] = {
                function()
                    require'dap'.continue()
                end
            },
        }
    }
}
return module
