
local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local mason_lspconfig = require "mason-lspconfig"

-- lspservers with default config
local servers = {
    "html",
    "cssls",
    "clangd",
    "emmet_ls",
    "jdtls",
    "luau_lsp",
    "lua_ls",
    "ts_ls",
    "rust_analyzer",
    "volar",
    "tailwindcss",
    "kotlin_language_server",
    "angularls",
    "solargraph", -- ruby ls
    "fsautocomplete",
    "omnisharp",
}

mason_lspconfig.setup {
    ensure_installed = servers
}

local lspconfig = require "lspconfig"

for _, lsp in ipairs(servers) do
     lspconfig[lsp].setup {
         on_attach = on_attach,
         capabilities = capabilities,
     }
end

lspconfig["pylsp"].setup {
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        pylsp = {
            plugins = {
                pycodestyle = {
                    enabled = false
                },
                jedi = {
                    environment = "/usr/bin/python"
                },
            }
        }
    }
}

table.insert(servers, "pylsp")

return servers

