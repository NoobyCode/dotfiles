
return {
    { "fsharp/vim-fsharp", lazy = false },
    { "elkowar/yuck.vim", lazy = false },
    { "williamboman/mason-lspconfig.nvim" },
    -- { "habamax/vim-godot", lazy = false },
    {
        "andweeb/presence.nvim",
        lazy = false,
        config = function()
            require "custom.configs.presence"
        end,
    },
    {
        "theRealCarneiro/hyprland-vim-syntax",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        ft = "hypr",
	},
    {
        "neovim/nvim-lspconfig",
        dependencies = "williamboman/mason-lspconfig.nvim",
        config = function()
            require "plugins.configs.lspconfig"
            require "custom.configs.lspconfig"
        end,
    },
    {
        "mfussenegger/nvim-dap",
        lazy = false,
        config = function()
            require "custom.configs.nvim-dap"
        end
    },
    {
        "jay-babu/mason-nvim-dap.nvim",
        config = function()
            require "custom.configs.mason-nvim-dap"
        end
    },
    -- {
    --     "rcarriga/nvim-dap-ui",
    --     lazy = false,
    --     dependencies = "mfussenegger/nvim-dap",
    --     config = function()
    --         require "custom.configs.nvim-dap-ui"
    --     end
    -- },

    -- overrides
    {
        "williamboman/mason.nvim",
        dependencies = { "williamboman/mason-lspconfig.nvim", "jay-babu/mason-nvim-dap.nvim" }
    },
    {
        "nvim-tree/nvim-tree.lua",
        after = "nvim-web-devicons",
        opts = {
            git = {
                enable = true
            },
            view = {
                width = 250
            },
            actions = {
                open_file = {
                    quit_on_open = true
                }
            }
        }
    },
}
