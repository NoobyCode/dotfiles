
local Color = {}
Color.__index = Color

local clamp = require "custom.lib".clamp

function Color.new(r, g, b)
    local self = setmetatable({}, Color)
    self.r = r
    self.b = b
    self.g = g
    return self
end

function Color.fromRGB(r, g, b)
   return Color.new(r/255, g/255, b/255)
end

function Color.fromHex(str)
    str = string.upper(str)
    -- validate hex string
    if not string.match(str, "#[0-9|A-F][0-9|A-F][0-9|A-F][0-9|A-F][0-9|A-F][0-9|A-F]") then
        error(str.." is not a valid hex color code string")
    end

    local matches = string.gmatch(str, "[0-9|A-F][0-9|A-F]")

    local colors = {}

    for color in matches do
        table.insert(colors, tonumber(color, 16)/255)
    end

    -- Future-proofing for when neovim updates their version of Lua
    if table.unpack then
        return Color.new(table.unpack(colors))
    end
    return Color.new(unpack(colors))
end

function Color:toHexString()
    return "#" ..
        string.format("%x", self.r*255) ..
        string.format("%x", self.g*255) ..
        string.format("%x", self.g*255)
end

function Color:lightened(f)
    return Color.new(
        clamp(0, self.r + (self.r * f), 1),
        clamp(0, self.g + (self.g * f), 1),
        clamp(0, self.b + (self.b * f), 1)
    )
end

function Color:darkened(f)
    return Color.new(
        clamp(0, self.r - (self.r * f), 1),
        clamp(0, self.g - (self.g * f), 1),
        clamp(0, self.b - (self.b * f), 1)
    )
end

return Color

