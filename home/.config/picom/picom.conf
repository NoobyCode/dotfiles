
blur-method = "dual_kawase";
blur-strength = 2;
blur-kern = "3x3box";
backend = "glx";

refresh_rate = 165
vsync = true;
frame-pacing = false;

detect-rounded-corners = true;
unredir-if-possible = true;
detect-client-opacity = true;

detect-transient = true;
use-damage = true;
log-level = "warn";

rules = (
    # Window types
    {
        blur-background = false;
    },
    {
        ### 'window_type' tries to match all window types,
        ### but i only want to match the primary window type,
        ### so i select the attribute '_NET_WM_WINDOW_TYPE' manually
        ###
        ### also some windows dont declare their window type for some reason (mpv why!?!?),
        ### so animate those windows like normal windows i guess lmao???
        match = "_NET_WM_WINDOW_TYPE[0] = '_NET_WM_WINDOW_TYPE_NORMAL' || !_NET_WM_WINDOW_TYPE || window_type = 'dialog'";
        corner-radius = 10; 
        shadow = true;
        shadow-radius = 6;
        shadow-offset-x = -3;
        shadow-offset-y = -3;
        shadow-color = "#000000";
        animations = (
            {
                triggers = [ "open", "show" ];
                preset = "appear";
                duration = 0.1;
            },
            {
                triggers = [ "close", "hide" ];
                preset = "disappear";
                duration = 0.1;
            },
            {
                triggers = [ "geometry" ]; # me too :(

                d = 0.1

                scale-x = {
                    curve = "cubic-bezier(0.07, 0.65, 0, 1)";
                    duration = "d";
                    start = "window-width-before / window-width";
                    end = 1;
                };
                scale-y = {
                    curve = "cubic-bezier(0.07, 0.65, 0, 1)";
                    duration = "d";
                    start = "window-height-before / window-height";
                    end = 1;
                };
                shadow-scale-x = "scale-x";
                shadow-scale-y = "scale-y";
                offset-x = {
                    curve = "cubic-bezier(0.07, 0.65, 0, 1)";
                    duration = "d";
                    start = "window-x-before - window-x";
                    end = 0;
                };
                offset-y = {
                    curve = "cubic-bezier(0.07, 0.65, 0, 1)";
                    duration = "d";
                    start = "window-y-before - window-y";
                    end = 0;
                };
                shadow-offset-x = "offset-x";
                shadow-offset-y = "offset-y";
            }
        );
    },
    {
        match = "window_type = 'popup_menu' || window_type = 'tooltip' || window_type = 'utility'";
        corner-radius = 1;
        animations = (
            {
            	triggers = [ "open", "show" ];
                suppressions = ["geometry"];
                opacity = {
                    duration = 0.1;
                    start = 0;
                    end = 1;
                };
            },
            {
            	triggers = [ "close", "hide" ];
                opacity = {
                    duration = 0.1;
                    start = 1;
                    end = 0;
                };
            }
        );
    },
    # Docks
    {
        match = "window_type *= 'dock'";
        blur-background = true;
        shadow = true;
    },
    {
        match = "name = 'Eww - bar'";
        corner-radius: 1;
        shadow = true;
        full-shadow = true;
        shadow-radius = 6;
        shadow-offset-x = -3;
        shadow-offset-y = -3;
        shadow-color = "#000000";
    },
    {
        match = "name *= 'settings-menu'";
        corner-radius = 10; 
        animations = (
            {
            	triggers = [ "open", "show" ];
                suppressions = ["geometry"];
            	preset = "slide-in";
            	direction = "up";
                duration = 0.1;
            },
            {
            	triggers = [ "close", "hide" ];
            	preset = "slide-out";
            	direction = "up";
                duration = 0.1;
            }
        );
    },
    # Specific windows
    {
        match = "class_g = 'Alacritty'";
        blur-background = true;
    },
    {
        match = "class_g = 'Alacritty' && focused";
        opacity = 0.96;
    },
    {
        match = "class_g = 'Alacritty' && !focused";
        opacity = 0.9;
    },
    {
        match = "name *= 'rofi'"
        corner-radius = 10; 
        shadow = true;
        shadow-radius = 6;
        shadow-offset-x = -3;
        shadow-offset-y = -3;
        shadow-color = "#000000";
        animations = (
            {
            	triggers = [ "open", "show" ];
                suppressions = ["geometry"];
            	preset = "appear";
                duration = 0.1;
            },
            {
            	triggers = [ "close", "hide" ];
            	preset = "disappear";
                duration = 0.1;
            }
        );
    },
    {
        match = "name *= 'ibus-ui'";
        animations = (
            {
                triggers = [ "open", "close", "hide" ];
                duration = 0.025;
            },
            {
                triggers = [ "geometry" ];
                duration = 0.025;
            }
        )
    }
);


