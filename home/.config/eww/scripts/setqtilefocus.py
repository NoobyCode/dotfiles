#!/usr/bin/env python
from libqtile.command.client import InteractiveCommandClient
from sys import argv
from os import getenv

client = InteractiveCommandClient()

if getenv("XDG_CURRENT_DESKTOP") == "Hyprland":
    exit(1)

client.group[argv[1]].toscreen(0)
