
the_weather=$(curl 'https://wttr.in/?m&format=%c%t' -sf)

if [[ $the_weather != *C || $? != 0  ]]; then
    echo '  +??°C'
else
    echo $(echo $the_weather | xargs)
fi
