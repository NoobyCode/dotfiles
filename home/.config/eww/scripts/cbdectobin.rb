#!/usr/bin/env ruby
require 'json'

STDOUT.sync = true

$digits = {
  1 => "two",
  2 => "four",
  3 => "eight",
  4 => "hex",
  6 => "stack",
  8 => "byte",
  16 => "short",
  32 => "int",
  64 => "long",
  128 => "overlong",
  256 => "byteplex"
}

$digit_places = $digits.keys

def name_number(number)
  return "" if number.count("0") == number.length
  return "one" if number.to_i == 1

  num_len = number.length
  largest_double_power = $digit_places.select { |i| num_len > i }.last
  
  left = number[0...num_len - largest_double_power]
  right = number[num_len - largest_double_power..-1]

  l = name_number(left)
  r = name_number(right)
  m = $digits[largest_double_power]

  if l.length == 0
    l = ""
    m = ""
  end
  l = "" if l == "one"

  case [m, r]
  when ["two", "one"]
    m = ""
    r = "three"
  when ["four", "one"]
    m = ""
    r = "five"
  when ["four", "two"]
    m = ""
    r = "six"
  when ["four", "three"]
    m = ""
    r = "seven"
  end

  "#{l} #{m} #{r}".strip
end

def custom_separator(number)
  digits = number.to_s.chars

  pattern = [3, 1, 2, 2]
  
  result = []
  index = 0

  while !digits.empty?
    group_size = pattern[index % pattern.length]
    result << digits.pop(group_size).join.rjust(group_size, ".")
    index += 1
  end

  result.reverse.join(' ')
end

last_output = {}

loop do
  sleep 1.0/4
  begin
    clipboard = `xclip -selection primary -o`.to_i.abs
  rescue 
    next
  end
  num_str = clipboard.to_s(2)
  if last_output[:n] == clipboard or num_str.length > 64 then
    next
  end
  output = {
    n: clipboard,
    number: custom_separator(num_str.gsub("1", "!").gsub("0", ".")),
    name: name_number(num_str)
  }
  puts output.to_json 
  last_output = output
end

