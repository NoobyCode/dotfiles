#!/usr/bin/env ruby

require 'json'

home = ENV['HOME']
parent_dev = File.stat("#{home}/Vaults/mnt/").dev
last_output = nil

loop do
  vault_mount = Dir.children("#{home}/Vaults/mnt/")
  current_output = JSON.dump(
    vault_mount.select { |i| File.directory?("#{home}/Vaults/mnt/#{i}") }
               .map { |i| { name: i,
                            isMounted: File.stat("#{home}/Vaults/mnt/#{i}").dev != parent_dev } }
  )

  if current_output != last_output
    system "echo", current_output # idk why but deflisten only works with echo and not puts
    last_output = current_output
  end

  sleep 0.1
end
