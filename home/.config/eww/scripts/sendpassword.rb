#!/usr/bin/env ruby

require 'shellwords'

`eww update passwordStatus='sending'`
the_password = `eww get thePassword`.strip
callback = `eww get passwordCallback`

if the_password.length == 0
  sleep 1
  `eww update passwordStatus='failed'`
end

the_password = Shellwords.shellescape the_password

result = system "#{callback % [the_password]}"

if not result
  `eww update passwordStatus='failed'`
else
  `eww update passwordStatus='success'`
  `eww close password-window`
end

