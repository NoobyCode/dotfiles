#!/usr/bin/env ruby

require 'json'

def parse_nmcli_output(output)
  lines = output.split("\n")
  networks = lines.map do |line|
    status, frequency, ssid = line.split(':')
    { frequency: if frequency[0] == '5' then "5 GHz" else "2.4 GHz" end,
      name: ssid,
      state: status == "yes" }
  end
  # some networks can have empty ssids apparently
  networks = networks.filter { |x| not x[:name].nil? }
  networks.sort_by { |x| x[:name] }
end

def get_wifi_networks
  output = `nmcli -t -f active,freq,ssid dev wifi`
  wifi_networks = parse_nmcli_output(output)
  output = `nmcli -t -f NAME connection show`.strip.split "\n"
  networks = wifi_networks.map do |x|
    c = x.clone
    c[:remembered] = output.include? c[:name]
    c
  end
  networks.sort_by { |x| x[:remembered] ?0:1 }.sort_by { |x| x[:state] ?0:1 }
end

last_output = ""

$stdout.sync = true
loop do
  networks = get_wifi_networks
  output_json = networks.to_json

  if output_json != last_output
    puts output_json
  end

  last_output = output_json
  sleep 0.1
end

