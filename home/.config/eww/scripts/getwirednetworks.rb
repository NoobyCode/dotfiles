#!/usr/bin/env ruby

require 'json'

def parse_nmcli_output(output)
  lines = output.split("\n")
  lines.flat_map do |line|
    name, network_type, state = line.split(':')
    if network_type.end_with? 'ethernet'
      [{ name: name, state: state == 'activated' }]
    else
      []
    end
  end
end

def get_wifi_networks
  output = `nmcli -t -f name,type,state connection show`
  parse_nmcli_output(output)
end

last_output = ""

loop do
  networks = get_wifi_networks
  output_json = networks.to_json

  if output_json != last_output
    system "echo", output_json
  end

  last_output = output_json
  sleep 0.1
end
