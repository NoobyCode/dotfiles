#!/bin/bash

prev_tab=$(eww get currentTab)
next_tab=$1
is_going_back=false

if [ "$2" == "-b" ]; then
  is_going_back=true
fi

eww update isGoingBack=$is_going_back
eww update prevTab=$prev_tab
eww update currentTab=$next_tab

