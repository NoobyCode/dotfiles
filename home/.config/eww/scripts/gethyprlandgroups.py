#!/usr/bin/env python
from hyprpy import Hyprland
from json import dumps
from time import sleep

instance = Hyprland()

def get_workspace_state(workspace):
    states = []
    if workspace.monitor.id == 0 and workspace.monitor.active_workspace_name == workspace.name:
        states.append('primary')
    elif workspace.monitor.active_workspace_name == workspace.name:
        states.append('visible')
    if len(workspace.windows) != 0:
        states.append('populated')
    else:
        states.append('unpopulated')
    return states

last_state = ""
while True:
    workspaces = [
        (i, (workspaces := next((j for j in instance.get_workspaces() if j.name == i), None)))
        for i in "123456789"
    ]
    state = dumps([
        {
            "name": workspace.name,
            "isPrimary": workspace.monitor.id == 0 and workspace.monitor.active_workspace_name == workspace.name,
            "isMax": workspace.has_fullscreen,
            "states": " ".join(get_workspace_state(workspace))
            
        } if workspace is not None else 
        {
            "name": name,
            "isPrimary": False,
            "isMax": False,
            "states": "unpopulated"
        }
        for name, workspace in workspaces
    ])

    if last_state != state:
        print(state, flush=True)
        last_state = state

    sleep(1/30)


