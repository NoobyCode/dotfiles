#!/usr/bin/env ruby
require 'json'

def convert_to_16_64_64_with_milliseconds(hours_24, minutes_60, seconds_60, milliseconds_1000)
  # Total milliseconds in 24:60:60 format
  total_ms_24 = (((hours_24 * 60 + minutes_60) * 60) + seconds_60) * 1000 + milliseconds_1000

  # Total milliseconds in a full 24-hour day
  total_ms_24_in_a_day = 24 * 60 * 60 * 1000

  # Total milliseconds in a full 16:64:64 day
  total_ms_16_in_a_day = 16 * 64 * 64 * 1000

  # Proportional conversion of total milliseconds
  total_ms_16 = (total_ms_24.to_f / total_ms_24_in_a_day) * total_ms_16_in_a_day

  # Convert total milliseconds back to hours, minutes, seconds, and milliseconds in the 16:64:64 format
  new_hours = (total_ms_16 / (64 * 64 * 1000)).to_i
  remaining_ms = total_ms_16 % (64 * 64 * 1000)

  new_minutes = (remaining_ms / (64 * 1000)).to_i
  remaining_ms = remaining_ms % (64 * 1000)

  new_seconds = (remaining_ms / 1000).to_i

  return new_hours, new_minutes, new_seconds
end

def to_binary(number)
  number.gsub("1", "■").gsub("0", "□")
end

STDOUT.sync = true
loop do
  time_24 = Time.new
  h, m, s = convert_to_16_64_64_with_milliseconds(time_24.hour, time_24.min, time_24.sec, time_24.usec/1000)
  clock = {
    hour: to_binary(h.to_s(2)).rjust(4, "□"),
    minute: to_binary(m.to_s(2)).rjust(6, "□"),
    second: to_binary(s.to_s(2)).rjust(6, "□"),
  }
  puts clock.to_json
  sleep(((24*60*60)/65536.0)/4)
end
