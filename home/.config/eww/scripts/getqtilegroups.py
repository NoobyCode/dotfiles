#!/usr/bin/env python
from time import sleep
from libqtile.command.client import InteractiveCommandClient
from json import dumps
from os import getenv
from sys import exit

client = InteractiveCommandClient()

if getenv("XDG_CURRENT_DESKTOP") == "Hyprland":
    exit(1)

def get_group_state(group):
    states = []
    if group['screen'] == 0:
        states.append('primary')
    elif group['screen'] is not None:
        states.append('visible')
    if len(group['windows']) != 0:
        states.append('populated')
    else:
        states.append('unpopulated')
    return states

last_state = ""
while True:
    groups = [client.group[i].info() for i in "123456789"]
    groups = [
        {
            "name": i['name'],
            "states": " ".join(get_group_state(i)),
            "isMax": i['layout'] == "max",
            "isPrimary": i['screen'] == 0
        }
        for i in groups
    ]
    state = dumps(groups)
    if last_state != state:
        
        print(state, flush=True)
        last_state = state
    sleep(1/30)
