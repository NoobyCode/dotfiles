# My dotfiles

All the config files that make my system mine

## Program overview

| Program | Name |
| ------- | ---- |
| terminal | [Alacritty](https://alacritty.org/) |
| shell | [fish](https://fishshell.com) |
| shell prompt | [Tide](https://github.com/IlanCosman/tide) |
| window manager | [Qtile](http://qtile.org) |
| compositor | [picom (next)](https://github.com/yshui/picom) |
| code editor | [neovim](https://neovim.io), [with NvChad config](https://nvchad.com) |
| notifications | [dunst](https://dunst-project.org) |
| screenshot | [Flameshot](https://flameshot.org/) |
| app launcher | [rofi](https://github.com/davatorium/rofi) |

note to self: do not forget to install git-lfs before bootstrapping
