#!/usr/bin/env bash

# taken from https://github.com/mathiasbynens/dotfiles
# (and modified)
# (and desemicolonified)
# (and detabified)

cd "$(dirname "${BASH_SOURCE}")";

function doIt() {
    cd ./home
    rsync --exclude "__pycache__" \
        --exclude ".mypy_cache" \
        -avh --no-perms . ~
    cd ..
}

if [[ $1 != "-y" ]]; then 
    read -p "This may overwrite existing files in your home directory. Are you sure? (y/n) " -n 1
else
    REPLY="y"
fi
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]; then
    doIt
fi
unset doIt
